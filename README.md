# Coding with Qiskit

### Description
In this repository you will get code related to the video tutorial [Coding with Qiskit.](https://www.youtube.com/playlist?list=PLOFEBzvs-Vvp2xg9-POLJhQwtVktlYGbY) [Qiskit](https://github.com/Qiskit) is an open-source framework for working with quantum computers.

### Software Dependencies
 * Python 3.5 or Later
 * Qiskit Framework
 * Jupyter notebook

### Quick Setup Guide
##### Update pip
```
pip install -U pip
```

##### Install qiskit
```
pip install qiskit
```
Watch [How to Install Qiskit](https://www.youtube.com/watch?v=M4EkW4VwhcI&list=PLOFEBzvs-Vvp2xg9-POLJhQwtVktlYGbY&index=3&t=0s) Video.

##### Install Visualization Functions
There are optional dependencies that are required to use all the visualization functions available in Qiskit. You can install these optional dependencies by with the following command
```
pip install qiskit-terra[visualization]
```
For more detailed information please read the documentation [Installing Qiskit](https://qiskit.org/documentation/install.html)

### Credits
* [Coding with Qiskit](https://www.youtube.com/playlist?list=PLOFEBzvs-Vvp2xg9-POLJhQwtVktlYGbY)  video tutorial.

* [Qiskit](Qiskit.bib)
